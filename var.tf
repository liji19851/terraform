variable "project" {
  default = "jerryli-tfc"
 }

variable "credentials_file" { default = "jerryli-tfc.json" }

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-c"
}

