provider "google" {
    region = "us-central1"
    zone    = "us-central1-c"
    project = "jerryli-tfc"
    credentials = file(var.credentials_file)
}

resource "google_compute_instance" "vm_instance" {
    count   =   3
    name    =   "terrform-vm${count.index}"
    machine_type    =   "e2-medium"

    boot_disk{
        initialize_params {
            image   =   "centos-cloud/centos-7"
        }
    }

network_interface {
    network     =   "default"
    access_config {
        
    }
}
}
